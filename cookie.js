
export const Cookie = Object.freeze({
	get: function (name) {
		const nameEQ = name + "=",
			  cookies = document.cookie.split(";");
		for(let i = 0, n = cookies.length, c ; i < n ; i++) {
			c = cookies[i];
			while(c.charAt(0) === " ")
				c = c.substring(1, c.length);
			if(c.indexOf(nameEQ) === 0)
				return decodeURIComponent(c.substring(nameEQ.length, c.length));
		}
		return null;
	},

	// path : laissez vide pour le dossier courant, "/" pour le site entier
	set: function(name, value, days, path, sameSite) { // fonction complétée par mes soins avec le paramètre 'path'
		let date, expires = "", str;

		if(path === undefined || path === null || path === ".")
			path = "";
		else
			path = "; path=" + (path === "/" ? "/" : encodeURIComponent(path));

		sameSite = "; SameSite=" + (sameSite || "Lax");

		if(days)
		{
			date = new Date();
			date.setTime(date.getTime()+(days*86400000)); // 24*60*60*1000
			expires = "; expires=" + date.toGMTString();
		}
		str = name + "=" + encodeURIComponent(value) + expires + path + sameSite;

		document.cookie = str;
	},
});

export default Cookie;
