<?php

$avatarDir = __DIR__;
$gladiators = array_filter(
    scandir($avatarDir),
    fn($g) => $g[0] !== '.' && $g !== '_invités' && is_dir("$avatarDir/$g")
);

$guestsDir = __DIR__.'/_invités';
$guests = array_filter(scandir($guestsDir), fn($g) => $g[0] !== '.');