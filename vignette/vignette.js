
import { avatarRoot, onGladiatorChange } from "/gladiateurs.js";
import { actions, getPicture, setActionsDisabled, saveFile } from "/commons.js";

const icons = document.querySelectorAll("img[src*=\".svg\"]");
const aside = document.querySelector("aside");

const mainFace = document.querySelector("#gladiator .face");
const secondaryFace = document.querySelector("#gladiator2 .face");

const getById = document.getElementById.bind(document);

const {classList: main} = document.querySelector("main");
actions["d"] = getById("mirror").onclick = () => main.toggle("right");


onGladiatorChange(({ name, background, iconBg, text }) => {
	document.body.style.color = text || iconBg;
	getById("bandeau").style.backgroundImage = `url("${avatarRoot}/${name}/bandeau_vignette.png")`;
	mainFace.style.backgroundImage = `url("${avatarRoot}/${name}/Logo.png")`;

	const colors = `?color=${background.replace("#", "%23")}&sColor=${iconBg.replace("#", "%23")}`;
	for(const icon of icons)
	{
		const { src, dataset } = icon;
		if(src.includes("color="))
		{
			icon.src = src.substring(0, src.indexOf("?"));
			for(const alt of icon.alts)
				dataset[alt] = dataset[alt].substring(0, dataset[alt].indexOf("?"));
		}

		icon.src += colors;
		for(const alt of icon.alts)
			dataset[alt] += colors;
	}

	for(const elm of document.querySelectorAll("[icon]"))
		elm.style.backgroundColor = aside.contains(elm) ? iconBg : background;
});


const friendModal = getById("friendModal");
getById("gladiator2").onclick
	= actions["m"]
	= friendModal.showModal.bind(friendModal);

for(const friend of document.getElementsByClassName("friend"))
	friend.onclick = chooseFriend;

function chooseFriend()
{
	secondaryFace.style.backgroundImage = `url(${this.src || ""})`;
	if(this.src)
		getById("gladiator2").removeAttribute("no-print");
	else
		getById("gladiator2").setAttribute("no-print", "");
	friendModal.close();
}

friendModal.addEventListener("click", function(event) {
	if(event.target === this)
		this.close();
});



/* ///////////////////////////////////////////////////////////////////// */
/* //////////////////////// Séries de vignettes //////////////////////// */
/* ///////////////////////////////////////////////////////////////////// */

import "/jszip.min.js";

const progress = document.querySelector("progress");

actions["s"] = saveSeries;
getById("save-series").onclick = saveSeries;

async function saveSeries()
{
	const prompt = window.prompt("Combien de vignettes générer ?\nPar exemple 10 ou 20-30");
	if(!prompt)
		return;

	let [start, finish] = prompt.split("-").map(Number);
	if(!start || start < 0)
	{
		window.alert("Entrée invalide. Veuillez donnez un nombre ou un fourchette de nombres.");
		return saveSeries();
	}
	
	if(!finish)
	{
		finish = start;
		start = 1;
	}
	if(finish < start)
	{
		finish ^= start;
		start ^= finish;
		finish ^= start;
	}

	const total = finish - start + 1;
	if(total > 100 && !confirm(`Vous vous apprêtez à générer ${total} vignettes.\nConfirmer ?`))
		return;

	setActionsDisabled(true);

	progress.max = total;
	progress.value = 0;
	progress.hidden = false;
	
	const category = getById("categorie");
	const categoryImg = category.removeChild(category.firstElementChild);
	const zip = new JSZip();

	for(let i = start ; i <= finish ; i++)
	{
		if(i < 10)
			i = "0" + i;
		else if(i >= 100)
			category.style.aspectRatio = 1.15;
		category.innerHTML = i;
		zip.file(`vignette${i}.png`, await getPicture(true));
		progress.value++;
	}
	category.style.removeProperty("aspect-ratio");
	
	zip.generateAsync({
		type: "blob", streamFiles: true,
		compression: "STORE",
	}).then(blob =>	{
		saveFile("vignettes.zip", URL.createObjectURL(blob));
		progress.hidden = true;
	});

	category.innerHTML = "";
	category.appendChild(categoryImg);
	setActionsDisabled(false);
}
