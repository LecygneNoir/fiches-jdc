<!doctype html>
<html lang="fr-FR">
<head>
	<meta charset="utf-8" />
	<meta property="og:title" content="Générateur de vignettes pour Les Jeux du Cygne" />
	<meta property="author" content="Morgân von Brylân" />

	<title>Vignettes</title>
	<link rel="icon" type="image/png" href="favicon.png" />

	<style><?php readfile(__DIR__.'/vignette.css'); ?></style>

	<script type="module" src="vignette.js"></script>
	<script type="module" src="/upload.js"></script>
</head>
<body>
	<main>
		<aside id="actions" no-print>
			<button id="save" no-print>Enregistrer (Ctrl+P)</button>
			<button id="save-series" no-print>Enregistrer une série (Ctrl+S)</button>
			<button id="mirror" no-print>Gauche/droite (Ctrl+D)</button>
		</aside>
		
		<aside id="bandeau">
			<div id="gladiator" title="Changer lia gladiateur·rices"><div class="face"></div></div>
			<div id="gladiator2" title="Mettre un·e invité·e" no-print><div class="face"></div></div>
		</aside>

		<div icon id="categorie"><img src="/icones/vignettes/decouverte.svg" /></div>

		<img id="logo" src="/LJDC.png" />

		<progress no-print hidden></progress>
	</main>

	<dialog id="friendModal" no-print>
		<section>
			<h1>Choisir un·e invité·e</h1>
			<div id="friends">
				<div class="friend" title="Aucun">[aucun]</div>
				<?php
					require '../avatars/list.php';
					$count = count($gladiators);
					$i = 0;
					foreach($gladiators as $gladiator)
					{
						echo <<<HTML
						<img class="friend" src="/avatars/$gladiator/Logo.png" title="$gladiator" />
HTML;
						if(++$i === (int)($count / 2))
							echo '<br />';
					}
					echo '<br />';
					foreach($guests as $guest)
					{
						$name = substr($guest, 0, -4);
						echo <<<HTML
						<img class="friend" src="/avatars/_invités/$guest" title="$name" />
HTML;
					}
				?>
			</div>
		</section>
	</dialog>
</body>
</html>