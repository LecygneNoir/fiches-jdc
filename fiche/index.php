<!doctype html>
<html lang="fr-FR">
<head>
	<meta charset="utf-8" />
	<meta property="og:title" content="Générateur de fiches de présentation pour Les Jeux du Cygne" />
	<meta property="author" content="Morgân von Brylân" />

	<title>Fiche de présentation</title>
	<link rel="icon" type="image/png" href="favicon.png" />

	<style><?php readfile(__DIR__.'/fiche.css'); ?></style>

	<script type="module" src="fiche.js"></script>
	<script type="module" src="/upload.js"></script>
</head>
<body>
	<div id="bandeau">
		<button id="save" no-print>Enregistrer (Ctrl+P)</button>

		<aside>
			<div id="gladiator" title="Changer lia gladiateur·rices"><div id="face"></div></div>
			<div icon><img toggle title="Support manette" src="/icones/controller.svg" data-alt1="/icones/no_controller.svg" /></div>
			<div icon><img switch src="/icones/players_single.svg" title="Nombre de joueurs"
				data-title0="Solo"
				data-alt1="/icones/players_coop.svg" data-title1="Coop"
				data-alt2="/icones/players_versus.svg" data-title2="Versus"
				data-alt3="/icones/players_multi.svg" data-title3="Multi" /></div>
			<div icon><img toggle src="/icones/fr" title="Français disponible" /></div>
		</aside>

		<progress no-print hidden></progress>

		<footer>
			<h1 id="genre" contenteditable spellcheck="false">genre</h1>
			<img id="earlyAccess" src="/icones/early.svg" hidden />
			<div contenteditable spellcheck="false" id="price">00,00€</div>
		</footer>
	</div>
</body>
</html>
