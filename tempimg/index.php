<?php

if($_SERVER['REQUEST_METHOD'] !== 'POST')
{
	header('Allow: OPTIONS, GET, POST', true, $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ? 200 : 405);
	exit;
}

header('Content-Type: text/plain');

const idir = __DIR__ . '/i/';

if(!is_dir(idir))
	mkdir(idir);

foreach(scandir(idir) as $file)
{
	$file = idir . $file;
	if(!is_file($file)) continue;

	$age = time() - stat($file)[9];
	var_dump($age);
	if($age > 24*3600)
		unlink($file);
}

$headers = apache_request_headers();
if(empty($headers['Content-Type']) || !str_starts_with($headers['Content-Type'], 'image/'))
{
	header('Accept: image/*', true, 415); // Unsupported Media Type
	echo 'Only images are allowed';
	exit;
}

if(empty($headers['Content-Length']))
{
	http_response_code(422); // Unprocessable entity
	echo 'Image cannot be empty';
	exit;
}
if($headers['Content-Length'] > 50000000)
{
	http_response_code(413);
	echo 'Image is too large (>50MB)';
	exit;
}

$filename = time().'.'.substr($headers['Content-Type'], 6);

if(!copy('php://input', idir.$filename))
{
	http_response_code(500);
	echo 'Failed to copy the file';
}
else
{
	http_response_code(201);
	header("Location: $_SERVER[REQUEST_URI]/i/$filename");
}