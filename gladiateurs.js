
export const gladiateurices = {
	cygne: { name: "Lecygne",
		background: "#D4302E", iconBg: "white",
	},

	chobitty : { name: "Chobitty",
		background: "#E82D41", iconBg: "white",
	},

	damn3d: { name: "Damn3d",
		background: "#F8AD38", iconBg: "#4A4E61",
	},

	morgân: { name: "Morgân",
		background: "#3A4885", iconBg: "#DAA3CB",
	},

	mrgecko: { name: "Gecko",
		background: "#FF8E2E", iconBg: "#202020",
	},

	akatsuki: { name: "Akatsuki",
	 	background: "#242424", iconBg: "#E80015",
	},

	zykino: { name: "Zykino",
		background: "#CCCCCC", iconBg: "#5A5A5A",
	},

	xébus: { name: "Xébus",
		background: "#378A94", iconBg: "#AAAAAA",
	},

	gaelden: { name: "Gaelden",
		background: "#814D13", iconBg: "white",
	},

	khat0x: { name: "Khat0x",
		background: "#3D303D", iconBg: "#F0653D",
	},

	ethernhell: { name: "EthernHell",
		background: "#626F65", iconBg: "#7E241F",
	},
};
export default gladiateurices;


export const noms = Object.keys(gladiateurices);

gladiateurices.morgan = gladiateurices.morgân;
gladiateurices.gecko = gladiateurices.mrgecko;
gladiateurices.xebus = gladiateurices.xébus;

export const avatarRoot = "/avatars";

import Cookie from "/cookie.js";

const modal = document.createElement("dialog");
let onChange = Function();
export function onGladiatorChange(handler)
{
	if(typeof handler !== "function")
		throw new TypeError("Handler must be a function.");
	onChange = handler;
	handler(gladiateurices[Cookie.get("gladiateurice") || "cygne"]);
}

function changeTo(gladiator)
{
	Cookie.set("gladiateurice", gladiator, 365, "/");
	onChange(gladiateurices[gladiator]);
	modal.close();
}


document.body.appendChild(modal);
modal.id = "gladiatorModal";
modal.innerHTML = `<section>
		<h1>Choix de Gladiateur</h1>
		<div id="gladiateurs"></div>
	</section>`;
const gladiateurs = document.getElementById("gladiateurs");
let i = 0;
for(const gladiator of noms)
{
	const { name } = gladiateurices[gladiator];
	gladiateurs.appendChild(Object.assign(document.createElement("img"), {
		src: `/avatars/${name}/Logo.png`,
		onclick: changeTo.bind(null, gladiator),
		className: "gladiator",
		title: name,
	}));
	if(++i === ~~(noms.length / 2))
		gladiateurs.appendChild(document.createElement("br"));
}

modal.addEventListener("click", function(event) {
	if(event.target === this)
		this.close();
});

const styles = document.createElement("style");
styles.textContent = `
	@import url("/dialog.css");
	
	#gladiatorModal .gladiator {
		margin-left: 10px;
	}
	#gladiatorModal .gladiator:first-child,
	#gladiatorModal br + .gladiator {
		margin-left: 0;
	}
	
	#gladiatorModal .gladiator {
		cursor: pointer;
		width: 100px;
		aspect-ratio: 1;
		box-sizing: border-box;
		display: inline-block;
	}`;
document.head.appendChild(styles);

const changeButton = document.getElementById("gladiator");
if(changeButton)
	changeButton.onclick = modal.showModal.bind(modal)
