<?php

if(empty($_GET['url']))
{
	http_response_code(422);
	exit;
}

$ch = curl_init($_GET['url']);
curl_setopt_array($ch, [
	CURLOPT_HEADER => false,
	CURLOPT_HTTP_CONTENT_DECODING => false,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_MAXFILESIZE_LARGE => 3e7, // 30 Mo
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_FAILONERROR => true,

	CURLOPT_HEADERFUNCTION => function($ch, $header) {
		if(!str_starts_with(strtolower($header), 'access-control'))
			header($header);
		return strlen($header);
	}
]);

curl_exec($ch);
curl_close($ch);