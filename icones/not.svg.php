<?php require '../svg.php'; ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="130" height="130" viewBox="0 0 130 130" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;">
    <g transform="matrix(1,0,0,1,-138.251,-396.575)">
        <g transform="matrix(1,0,0,1,10,-25)">
            <g transform="matrix(0.26801,0,0,0.26801,126.089,419.414)">
                <path d="M250,8.064C116.473,8.064 8.065,116.472 8.065,249.999C8.065,383.527 116.473,491.934 250,491.934C383.528,491.934 491.935,383.527 491.935,249.999C491.935,116.472 383.528,8.064 250,8.064ZM370.659,105.003C412.201,139.613 438.665,191.74 438.665,249.999C438.665,354.126 354.127,438.664 250,438.664C227.061,438.664 205.073,434.561 184.726,427.048L370.659,105.003ZM322.473,75.778L135.384,399.825C90.375,365.327 61.336,311.028 61.336,249.999C61.336,145.872 145.873,61.335 250,61.335C275.678,61.335 300.164,66.476 322.473,75.778Z" style="fill:rgb(255,0,0);"/>
            </g>
        </g>
    </g>
</svg>
